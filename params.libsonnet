local env = std.extVar('qbec.io/env');

local paramsMap = {
  webapp: import './environments/webapp.libsonnet',
};

if std.objectHas(paramsMap, env) then paramsMap[env] else error 'environment ' + env + ' not defined in ' + std.thisFile
