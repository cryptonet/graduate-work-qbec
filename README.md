# Qbec manifests for deployment webapp application

## Requirements
* [Kubectl](https://kubernetes.io/docs/reference/kubectl/) must be installed and configured with access to the cluster.
* [Qbec](https://qbec.io/) must be installed.

## Quick start
```yaml
qbec apply webapp --yes --wait
```

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
