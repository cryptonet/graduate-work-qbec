local p = import '../params.libsonnet';
local params = p.components.webapp;

[
  {
    apiVersion: "v1",
    kind: "Namespace",
    metadata: {
       name: "webapp"
    }
  },
  {
    kind: "Deployment",
    apiVersion: "apps/v1",
    metadata: {
      name: "webapp",
      namespace: "webapp",
      labels: {
        app: "webapp"
      }
    },
    spec: {
      replicas: params.replicas,
      selector: {
        matchLabels: {
          app: "webapp"
        }
      },
      template: {
        metadata: {
          labels: {
            app: "webapp"
          }
        },
        spec: {
          containers: [
            {
              name: "webapp",
              image: "cryptodeveloper/graduate-work-webapp:" + params.version,
              imagePullPolicy: "IfNotPresent",
              ports: [
                {
                  name: "http",
                  containerPort: 80
                }
              ],
            }
          ],
          imagePullSecrets: [
            {
              name: "gitlab-registry"
            }
          ]
        }
      }
    }
  },
  {
    kind: "Service",
    apiVersion: "v1",
    metadata: {
      name: "webapp",
      namespace: "webapp",
      labels: {
        app: "webapp"
      }
    },
    spec: {
      type: "LoadBalancer",
      selector: {
        app: "webapp"
      },
      ports: [
        {
          name: "http",
          port: 80,
          targetPort: 80
        }
      ]
    }
  }
]
